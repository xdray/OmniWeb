#ifndef		__OMNI_RESPONSE_H__
#define		__OMNI_RESPONSE_H__

#include	<map>
#include	<string>

struct MHD_Connection;

class Response {
	typedef std::map<std::string, std::string> HeaderMap;

public:
	Response(MHD_Connection * p) : _pConn(p), _nCode(200), _mHeader(), _sContent() { Header("Content-type", "text/plain"); }

	void	Header(const std::string & sHeader, const std::string & sValue) { _mHeader[sHeader] = sValue; }
	void	Echo(const std::string & sContent) { _sContent.append(sContent); }
	void	Send();

	void	Error(int nCode);
	void	Redirect(const std::string & sUrl);
	void	File(const std::string & sUrl);

private:
	MHD_Connection *	_pConn;
	int					_nCode;
	HeaderMap			_mHeader;
	std::string			_sContent;
};

#endif//!	__OMNI_RESPONSE_H__