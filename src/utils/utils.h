#ifndef		__OMNI_UTILS_H__
#define		__OMNI_UTILS_H__

#include	<string>

/**
 * Get current tick(milliseconds)
 */
extern double Tick();

/**
 * Generate unique id string.
 */
extern std::string CreateID();

/**
 * Check if a Path/File exists.
 */
extern bool Exists(const char * sPath);

/**
 * Make dir
 */
extern void MakeDir(const char * sPath);

/**
 * Get HTTP GMT time string.
 */
extern std::string GMTime(uint32_t nUnixTimestamp);

#endif//!	__OMNI_UTILS_H__
