#ifndef		__OMNI_COMMAND_H__
#define		__OMNI_COMMAND_H__

#include	<map>
#include	<string>

class Command {
public:
	Command() {}
	Command(int nArgc, char * pArgv[]) { Parse(nArgc, pArgv); }
	Command(const std::string & sCmd) { Parse(sCmd); }

	void		Parse(int nArgc, char * pArgv[]);
	void		Parse(const std::string & sCmd);
	bool		Has(const std::string & sKey);
	std::string	Get(const std::string & sKey);

private:
	std::map<std::string, std::string>	_mCmd;
};

#endif//!	__OMNI_COMMAND_H__
