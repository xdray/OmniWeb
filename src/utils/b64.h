#ifndef		__OMNI_B64_H__
#define		__OMNI_B64_H__

enum base64_encodestep {
	step_A, step_B, step_C
};

struct base64_encodestate {
	base64_encodestep step;
	char result;
	int stepcount;
};

enum base64_decodestep {
	step_a, step_b, step_c, step_d
};

struct base64_decodestate {
	base64_decodestep step;
	char plainchar;
};

void base64_init_encodestate(base64_encodestate* state_in);
char base64_encode_value(char value_in);
int base64_encode_block(const char* plaintext_in, int length_in, char* code_out, base64_encodestate* state_in);
int base64_encode_blockend(char* code_out, base64_encodestate* state_in);

void base64_init_decodestate(base64_decodestate* state_in);
int base64_decode_value(char value_in);
int base64_decode_block(const char* code_in, const int length_in, char* plaintext_out, base64_decodestate* state_in);

#endif//!	__OMNI_B64_H__
