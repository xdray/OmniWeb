#include	"storage.h"
#include	"../utils/utils.h"

Storage & Storage::Get() {
	static Storage * pIns = nullptr;
	if (pIns == nullptr) pIns = new Storage;
	return *(pIns);
}

std::string Storage::Get(const std::string & sKey) {
	auto it = _mCache.find(sKey);
	if (it == _mCache.end()) return "";

	double nCur = Tick();
	if (it->second.nExpire > 0 && nCur >= it->second.nExpire) {
		_mCache.erase(it);
		return "";
	} else {
		if (it->second.nExpire > 0) it->second.nExpire = nCur + _nExpire;
		return it->second.sValue;
	}
}

void Storage::Set(const std::string & sKey, const std::string & sValue, bool bNeverExpire) {
	if (sValue.empty()) {
		_mCache.erase(sKey);
	} else {
		Data iData;
		iData.sValue = sValue;
		iData.nExpire = bNeverExpire ? -1 : _nExpire + Tick();
		_mCache[sKey] = iData;
	}
}

