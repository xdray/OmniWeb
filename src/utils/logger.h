#ifndef		__OMNI_LOGGER_H__
#define		__OMNI_LOGGER_H__

#include	<string>
#include	<mutex>

#define		GLog	Logger::Get()

class Logger {
public:
	Logger();
	virtual ~Logger();

	static Logger &	Get();

	void	Error(const char * pFmt, ...);
	void	Warn(const char * pFmt, ...);
	void	Info(const char * pFmt, ...);
	void	Debug(const char * pFmt, ...);
	
private:
	char *		_pBuf;
	std::mutex	_iLock;
};

#endif//!	__OMNI_LOGGER_H__
