#ifndef		__OMNI_STORAGE_H__
#define		__OMNI_STORAGE_H__

#include	<map>
#include	<string>

class Storage {
	struct Data {
		std::string	sValue;
		double		nExpire;
	};

public:
	Storage() : _nExpire(900000), _mCache() {}

	static Storage &	Get();

	void		Expire(double n) { _nExpire = n; }
	std::string Get(const std::string & sKey);
	void		Set(const std::string & sKey, const std::string & sValue, bool bNeverExpire = false);

private:
	double						_nExpire;
	std::map<std::string, Data>	_mCache;
};

#endif//!	__OMNI_STORAGE_H__
