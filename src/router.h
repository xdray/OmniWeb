#ifndef		__OMNI_ROUTER_H__
#define		__OMNI_ROUTER_H__

#include	<regex>
#include	<string>
#include	<vector>

class Router {
	struct Api {
		std::regex	iRouter;
		std::string	sMethod;
		int			nProcRef;
	};

public:
	Router() : _vApis() {}

	static Router &	Get();

	void	Register(const std::string & sMethod, const std::string & sRouter, int nProcRef);
	int		Match(const std::string & sMethod, const std::string & sUrl, int & nProc, std::smatch & rMatch);

private:
	std::vector<Api>	_vApis;
};

#endif//!	__OMNI_ROUTER_H__
