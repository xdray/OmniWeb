#pragma once

#include	<microhttpd.h>
#include	<regex>
#include	<string>

struct lua_State;

class Omni {
public:
	Omni();
	virtual ~Omni();

	void		Start(int nArgc, char * pArgv[]);
	void		Serve(
		MHD_Connection * pConn,
		const char * pMethod,
		const char * pUrl,
		int nProc,
		std::smatch & rMatch,
		void ** pConnData);

private:
	void		__PrepareRequest(lua_State *, MHD_Connection *, const char *, const char *, void **);
	void		__PrepareResponse(lua_State *, class Response *);
	void		__PrepareExtra(lua_State *, std::smatch &);

private:
	MHD_Daemon *	_pServer;
	lua_State *		_pL;
};