extern "C" {
#include	<luajit.h>
#include	<lualib.h>
#include	<lauxlib.h>
}

#include	"../utils/logger.h"

namespace LuaApi { namespace LuaLogger {
	static int Error(lua_State * pL) {
		const char * pMsg = lua_tostring(pL, 1);
		GLog.Error(pMsg);
		return 0;
	}

	static int Warn(lua_State * pL) {
		const char * pMsg = lua_tostring(pL, 1);
		GLog.Warn(pMsg);
		return 0;
	}

	static int Info(lua_State * pL) {
		const char * pMsg = lua_tostring(pL, 1);
		GLog.Info(pMsg);
		return 0;
	}

	static int Debug(lua_State * pL) {
		const char * pMsg = lua_tostring(pL, 1);
		GLog.Debug(pMsg);
		return 0;
	}
} }

void RegisterApi_Logger(lua_State * pL) {
	lua_newtable(pL);

	lua_pushcfunction(pL, &LuaApi::LuaLogger::Error);
	lua_setfield(pL, -2, "error");
	lua_pushcfunction(pL, &LuaApi::LuaLogger::Warn);
	lua_setfield(pL, -2, "warn");
	lua_pushcfunction(pL, &LuaApi::LuaLogger::Info);
	lua_setfield(pL, -2, "info");
	lua_pushcfunction(pL, &LuaApi::LuaLogger::Debug);
	lua_setfield(pL, -2, "debug");

	lua_setglobal(pL, "log");
}