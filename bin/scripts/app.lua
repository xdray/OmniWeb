-- ========================================================
-- @File	: app.lua
-- @Brief	: 程序加载脚本入口
-- @Author	: Leo Zhao
-- @Date	: 2017-04-24
-- ========================================================

router.get('/', function(req, rsp)
	rsp:json{message = 'OK', data = {id = 1, name='Leo'}}
end);

router.any('^/([[:w:]]+)', function(req, rsp, controller)
	rsp:echo('hello');
end);

router.any('^/([[:w:]]+)/([[:w:]]+)', function(req, rsp, controller, action)
	rsp:error(403);
end);

