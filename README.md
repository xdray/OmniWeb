OmniWeb
----
LUA WEB服务器

## 特性

> 1. 小而快将是本软件的始终目标
2. 集成GNU libmicrohttpd(0.9.53), LuaJIT(2.1.0B2), lua-CJson. lua-resty-template

## 运行说明

支持启动命令行参数有：

| *参数*  | *说明* |
| --- | --- |
| -h, help | 显示使用说明 |
| port=[80] | 设置监听端口，默认80 |
| www=[www] | 设置静态内容的目录，默认www |
| upload=[upload] | 设置上传内容的目录，默认upload |
| daemon | 以Daemon方式后台启动 |

## 路由说明

1. 支持正则（C++版）路由
2. 接口

	| *接口*  | *说明* |
	| --- | --- |
	| router.get(filter, proc) | GET路由 |
	| router.post(filter, proc) | POST路由 |
	| router.put(filter, proc) | PUT路由 |
	| router.delete(filter, proc) | DELETE路由 |
	| router.any(filter, proc) | 无限制路由 |

3. 示例

```lua
-- 基本
router.get('/', function(req, rsp)
	rsp:html('<h1>HELLO</h1>');
end);

-- 正则取一个参数。如：访问{HOST}/user时，controller为'user'
router.get('^/([[:w:]]+)$', function(req, rsp, controller)
	log.info('controller : ' .. controller);
	rsp:json{ message = 'OK' };
end);

-- 正则取两个参数。如：访问{HOST}/user/index时，ctrl为'user', act为'index'
router.get('^/([[:w:]]+)/([[:w:]]+)$', function(req, rsp, ctrl, act)
end);

```

## Request表（路由处理函数proc的第一个参数）

| *键*  | *说明* |
| --- | --- |
| url | 访问的URL，如/home/index?aa=1中/home/index为url |
| method | HTTP method。GET/POST/PUT/DELETE |
| remote | 客户端ip |
| cookie | Cookie表 |
| get | QueryString参数表。如req.get.uid等 |
| post | POST请求的参数表 |
| file | 上传文件的真实路径表，如：req.file.kk为FORM中name=kk的FILE存放路径 |

## Response类（路由处理函数proc的第二个参数）

| *方法*  | *说明* |
| --- | --- |
| header(k, v) | 设置回应的HTTP头，如：rsp:header('Content-type', 'text/plain') |
| echo(str) | 写入内容。如：rsp:echo('ssss') |
| html(view[, data [, is_plain]]) | 写入内容并设置Content-type为text/html |
| json(table[, set_header]) | 写入Table为JSON内容，可选设置Content-type为application/json |
| error(code) | 发送错误信息。需要自行控制return |
| redirect(url) | 发送跳转。需要自行控制return |
| cookie(k, v[, expire[, path]]) | COOKIE设置 |
| flush() | 输出并清空写内容 |

## 视图

1. 语法请自行参考lua-resty-template。
2. 渲染调用接口为rsp:html(view, data, is_plain)。

| *参数*  | *是否必填* | *说明* |
| --- | --- | --- |
| view | 是 | HTML文本或html文件路径 |
| data | 否 | 生成动态页面需要的参数 |
| is_plain | 否 | view参数是否是HTML文本 |

## 全局存储

> 使用string => string的key-value存储

| *方法*  | *说明* |
| --- | --- |
| storage.get(k) | 取一个存放的value。不存在或过期时返回''。 |
| storage.set(k, v, never_expire) | 缓存一个string。 |

## Lua层的C++接口

全局函数  

* `uuid() -> string` 生成唯一ID
* `md5(string) -> string` md5一个string
* `random(int[, int]) -> double` 生成随机数
* `tick() -> double` 毫秒级CPU时间
* `inherit(table) -> table` 模拟继承
* `exists(f) -> bool` 测试一个文件或目录是否存在

JSON  

* `json.encode(v) -> string` 将LUA的变量转为JSON字串
* `json.decode(v) -> v` 将JSON字串解析为lua的变量 

BASE64  

* `b64.encode(s) -> string` Base64Encode
* `b64.decode(s) -> string` Base64Decode

LOGGER  

* `log.debug(s)` 输出DEBUG信息
* `log.info(s)`	输出INFO
* `log.warn(s)` 输出警告
* `log.error(s)` 输出错误

## GCC说明

> 4.8的C++11 Regex可能会有问题，请使用5.X编译

## MVC

附件中提供了Win32下的LUA Simple MVC的包，欢迎试用
